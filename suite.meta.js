/*!
// ==UserScript==
// @name         backpack.tf enhancement suite
// @namespace    http://steamcommunity.com/id/caresx/ http://steamcommunity.com/id/theoddball
// @author       cares with edits by The Oddball
// @version      1.5.1.2
// @description  Enhances your backpack.tf experience.
// @include      /^https?://.*\.?backpack\.tf/.*$/
// @exclude      /^https?://forums\.backpack\.tf/.*$/
// @require      https://caresx.github.io/backpacktf-enhancement-suite/deps.js
// @downloadURL  https://theoddball.github.io/oddBES/suite.user.js
// @updateURL    https://theoddball.github.io/oddBES/suite.meta.js
// @grant        GM_xmlhttpRequest
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// ==/UserScript==
*/
